# Reconocimiento de placas

Permite el reconocimiento de placas por medio de una imagen de un vehículo. Proyecto realizado para Sistemas Operativos, de la Universidad Autónoma de Chihuahua

## Requerimientos

+ Ruby: 2.3.1
+ Ruby on Rails: 5.1.4
+ PostgreSQL: 9.4.1 o superior.

## Instalación

### Dependencias del sistema

Antes de poder ejecutar la aplicación es necesario tener un ambiente de Ruby debidamente instalado. Se recomienda
instalar un administrador de versiones de Ruby, tal como [RVM](https://rvm.io/) o [RBENV](http://rbenv.org/).

Una vez configurado el ambiente de Ruby, instalar las gemas necesarias mediante la ejecución de los siguientes pasos en
la terminal:

+ Dirigirse al directorio raíz del proyecto: `$ cd <directorio raíz del proyecto>`
+ Instalar las gemas mediante: `$ bundle install`

### Configuración inicial

#### Base de datos

##### Creación

El proyecto está pensado para hacer uso de las configuraciones predeterminadas de Ruby on Rails, por lo que no es
necesario modificar el archivo database.yml, únicamente es necesario contar con una instalación de PostgreSQL y de un
superusuario con el mismo nombre que el usuario de la sesión. (En el caso de Mac Os X es más sencillo si se ha instalado
Postgresapp, ya que el superusuario se crea automáticamente).

En la terminal, en el directorio raíz del proyecto ejecutar los siguientes comandos:

+ `$ rails db:create`
+ `$ rails db:migrate`

##### Inicialización

De igual manera, en la terminal, dirigirse al directorio raíz del proyecto y ejecutar:

+ `$ rails db:seed`

## Uso

Antes que nada, asegurarse que todas las tecnologías requeridas se encuentren debidamente instaladas y en ejecución:

+ PostgreSQL.

En la terminal, en el directorio raíz del proyecto, ejecutar:

+ `$ rails server`
